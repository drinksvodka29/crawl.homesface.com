<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/crawl','CrawlController@index');
Route::get('/update/vitri','UpdateController@vitri');
Route::get('/update/option','UpdateController@updateOptionItem');
Route::get('/update/project','UpdateController@project');
Route::get('/update/projects','UpdateController@projects');
Route::get('/update/latLng','UpdateController@latLng');

// homedy
Route::get('/homedy/category','HomedyController@category');
Route::get('/homedy/place/{category_id}','HomedyController@place');

// chu dau tu
Route::get('/chudautu','ChuDauTuController@index');
Route::get('/chudautu/crawl','ChuDauTuController@crawl');
Route::get('/chudautu/image','ChuDauTuController@image');
Route::get('/chudautu/convert','ChuDauTuController@convert');

// fake data tourscomplete
Route::get('/tc/resources','ToursCompleteController@resources');
Route::get('/tc/projects','ToursCompleteController@projects');
Route::get('/tc/bookings','ToursCompleteController@bookings');

// quanlynha.gov.vn
Route::get('/qln/khuvuc','QLNController@khuvuc');
Route::get('/qln/donvi','QLNController@donvi');
Route::get('/qln/import','QLNController@import');

// datvangtructuyen
Route::get('/nvtt/nhathau','NVTTController@nhathau');
Route::get('/nvtt/import','NVTTController@import');

// chotot.com
Route::get('/chotot/crawl','ChoTotController@crawl');
Route::get('/chotot/crawlDetail','ChoTotController@crawlDetail');
Route::get('/chotot/import','ChoTotController@import');

// mogi.vn
Route::get('/mogi/crawl','MogiController@crawl');
Route::get('/mogi/crawlDetail','MogiController@crawlDetail');
Route::get('/mogi/import','MogiController@import');
Route::get('/mogi/importAddress','MogiController@importAddress');
