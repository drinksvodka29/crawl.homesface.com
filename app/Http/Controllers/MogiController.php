<?php

namespace App\Http\Controllers;

use App\Direction;
use App\Models\Mogi\Mogi;
use App\Models\Mogi\MogiDetail;
use App\RealEstate;
use App\RealEstateCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;

class MogiController extends Controller
{
    function importAddress(){
        $this->setConfigNoLimit();

        $mogis = Mogi::where('run',0)->get();

        foreach ($mogis as $mogi){
            if(empty($mogi)) die;
            $html =  $this->curl($mogi->url,'get');

            if(empty($html)) die;
            $dom = HtmlDomParser::str_get_html($html);

            $dt_ob = $dom->find('#breadcrumb li');
            $street = '';
            $category = '';
            foreach ($dt_ob as $id => $item){
                if($id == 4){
                    $arr = explode('/',$mogi->url);
                    $category = isset($arr[4]) ? $arr[4] : '';
                    $street = trim($item->plaintext);
                }
            }

            $mogi->run = 1;
            $mogi->save();

            MogiDetail::where('mogi_id',$mogi->id)->where('label',"category_code")->delete();
            MogiDetail::where('mogi_id',$mogi->id)->where('label',"category_name")->delete();
            MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>"category_name",'value'=>$street]);
            MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>"category_code",'value'=>$category]);
        }

//        $mogi_details = MogiDetail::where('label','address')->where('ad_run',0)->get();
//
//        foreach ($mogi_details as $detail){
//            $arr = explode(",",$detail->value);
//            $detail->street = isset($arr[0]) ? trim($arr[0]) : '';
//            $detail->ward = isset($arr[1]) ? trim($arr[1]) : '';
//            $detail->district = isset($arr[2]) ? trim($arr[2]) : '';
//            $detail->province = isset($arr[3]) ? trim($arr[3]) : '';
//            $detail->ad_run = 1;
//            $detail->save();
//        }

        echo 'done';
    }

    function getAddressNumber(){
        $rand = rand(1,10);
        if($rand <=3)
            $rs = rand(150,1500);
        else
            $rs = rand(150,1500)."/".rand(5,80);

        return $rs;
    }

    function import(){
        $this->setConfigNoLimit();
        $mogis = Mogi::where('import',0)->limit(13000)->get();

        foreach ($mogis as $mogi){
            //  $mogi = Mogi::where('import',0)->first();
            //    $mogi = Mogi::where('id',4)->first();
            if(empty($mogi)) die;

            $mogi_details = MogiDetail::where('mogi_id',$mogi->id)->get();

            $re = RealEstate::where('mogi_id',$mogi->mogi_id)->first();
            if(empty($re))
                $re = new RealEstate();
            else {
                $file = env('RESOURCE_PATH') . $re->cover_image;
                if (!empty($re->cover_image) && file_exists($file))
                    unlink($file);

                RealEstateCategory::where('real_estate_id',$re->id)->delete();
            }

            $cover_image = "";
            $district_name = '';
            $ward_name = '';
            $ac_1 = 0;
            $ac_2 = 0;

            foreach ($mogi_details as $detail){
                $label = $detail->label;
                $value = $detail->value;

                switch ($label){
                    case "Giá":
                        $arr = explode(" ",$value);
                        $price = 0;
                        foreach ($arr as $i => $item){
                            if($item == "tỷ")
                                $price += $arr[$i - 1] * 1000000000;
                            else if($item == "triệu")
                                $price += $arr[$i - 1] * 1000000;
                        }
                        $re->price = $price;
                        break;
                    case "address":
                        $re->address = $value;
                        $re->street = $value;
                        $ward_name = $value;
                        break;
                    case "province":
                        $province_code = '';
                        if($value == "Hà Nội")
                            $province_code = 'HN';
                        else if($value == "Bà Rịa - Vũng Tàu")
                            $province_code = "VT";
                        elseif($value = "TPHCM")
                            $province_code = "SG";
                        else{
                            $q = DB::select(
                                "SELECT code FROM province WHERE '$value' like CONCAT('%',name,'%')"
                            );
                            if(!empty($q))
                                $province_code = $q[0]->code;
                        }
                        $re->province_code = $province_code;
                        break;
                    case "district":
                        $district_name = $value;
                        break;
                    case "Diện tích đất":
                        $ac_1 = floatval($value);
                        break;
                    case "Diện tích sử dụng":
                        $ac_2 = floatval($value);
                        break;
                    case "Hướng":
                        $direction = Direction::where('name',$value)->first();
                        if(!empty($direction))
                            $re->direction_code = $direction->code;
                        break;
                    case "image_cover":
                        $cover_image = $value;
                        break;
//                    case "Ngày đăng":
//                        $re->created_at = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$value)));
//                        break;
                    case "Nhà tắm":
                        $re->bathroom_quantity = intval($value);
                        break;
                    case "Pháp lý":
                        if($value !== "Không xác định")
                            $re->house_certificate = 1;
                        break;
                    case "Phòng ngủ":
                        $re->bedroom_quantity = intval($value);
                        break;
                    case "title":
                        $re->title = $value;
                        $re->keyword = $value;
                        $slug = $this->slug($re->title);
                        if(empty($re->id) && RealEstate::where('slug',$slug)->exists())
                            $re->slug = $this->slug($re->title).'-'.time().'-'.rand(1,1000);
                        else
                            $re->slug = $this->slug($re->title);
                        break;
                    case "category_code":
                        $category_id = 0;
                        switch ($value){
                            case "mua-can-ho-chung-cu":
                            case "mua-can-ho-penthouse":
                            case "mua-can-ho-tap-the-cu-xa":
                                $category_id = 2;
                                break;
                            case "mua-can-ho-dich-vu":
                            case "mua-cua-hang-shop-shophouse":
                                $category_id = 6;
                                break;
                            case "mua-can-ho-officetel":
                                $category_id = 9;
                                break;
                            case "mua-dat-kho-xuong":
                                $category_id = 8;
                                break;
                            case "mua-dat-nen-du-an":
                            case "mua-dat-nong-nghiep":
                            case "mua-dat-tho-cu":
                                $category_id = 1;
                                break;
                            case "mua-duong-noi-bo":
                            case "mua-nha-hem-ngo":
                            case "mua-nha-mat-tien-pho":
                                $category_id = 3;
                                break;
                            case "mua-mat-bang-cua-hang-shop-nhieu-muc-dich":
                            case "mua-mat-bang-cua-hang-shop-quan-an-nha-hang":
                            case "mua-mat-bang-cua-hang-shop-spa-tiem-toc-nail":
                            case "mua-mat-bang-cua-hang-shop-cafe-do-uong":
                            case "mua-mat-bang-cua-hang-shop-thoi-trang-my-pham-thuoc":
                                $category_id = 17;
                                break;
                            case "mua-nha-biet-thu-lien-ke":
                                $category_id = 4;
                                break;
                            default:
                                break;
                        }

                        break;
                    default:
                        break;
                }
            }

            $re->acreage = !empty($ac_1) ? $ac_1 : $ac_2;

            $q = DB::select(
                "SELECT id FROM district WHERE ? like CONCAT('%',name,'%') AND province_code = ?"
                ,[$district_name,$re->province_code]);
            if(!empty($q))
                $re->district_id = $q[0]->id;

            $q = DB::select(
                "SELECT id FROM ward WHERE ? like CONCAT('%',_name,'%') AND _district_id = ?"
                ,[$ward_name,$re->district_id]);
            if(!empty($q))
                $re->ward_id = $q[0]->id;

            if(empty($re->acreage) || empty($re->province_code) || empty($re->district_id)){
                $mogi->import = 1;
                $mogi->save();
                continue;
            }

            $re->status = 1;
            $re->address_number = $this->getAddressNumber();
            $re->updated_at = date('Y-m-d H:i:s', time());
            $re->created_at = date('Y-m-d H:i:s', time());
            $re->mogi_id = $mogi->mogi_id;
            $re->body = $mogi->body;
            $re->save();
            $re->slug .= "-".$re->id;
            $re->save();

            try{
                $arr = explode('.',$cover_image);
                $ext = $arr[count($arr) - 1];
                $fName = $re->id."-".time().".$ext";
                $content = file_get_contents($cover_image);

                $re->cover_image = "real_estates/cover_images/$fName";
                $re->save();
                file_put_contents(env('RESOURCE_PATH')."real_estates/cover_images/$fName", $content);
            }catch (\Exception $exception){
//                var_dump($re->id,$re->cho_tot_id,$exception->getMessage());
//                die;
            }

            $mogi->import = 1;
            $mogi->save();

            if(!empty($category_id)){
                $real_estate_category = new RealEstateCategory();
                $real_estate_category->real_estate_id = $re->id;
                $real_estate_category->category_id = $category_id;
                $real_estate_category->save();
            }
        }

        echo 'done';
    }

    function crawlDetail(){
        $this->setConfigNoLimit();

        $mogis = Mogi::where('run',0)->get();

        foreach ($mogis as $mogi){
            if(empty($mogi)) die;
            $html =  $this->curl($mogi->url,'get');

            if(empty($html)) die;
            $dom = HtmlDomParser::str_get_html($html);

            $ic_ob = $dom->find('#top-media .media-item img',0);
            $image_cover = !empty($ic_ob) ? $ic_ob->src : '';

            $tt_ob = $dom->find('#main .header .title',0);
            $title = !empty($tt_ob) ? $tt_ob->plaintext : '';

            $ad_ob = $dom->find('#main .header .address',0);
            $address = !empty($ad_ob) ? $ad_ob->plaintext : '';

            $pr_ob = $dom->find('#main .header .price',0);
            $price = !empty($pr_ob) ? $pr_ob->plaintext : '';

            $dt_ob = $dom->find('#prop-info li');
            $details = [];
            $details['Giá'] = $price;
            foreach ($dt_ob as $item){
                $pl = trim($item->plaintext);
                $arr = explode(":",$pl);
                if(count($arr) > 1)
                    $details[trim($arr[0])] = trim($arr[1]);
            }

            $bd_ob = $dom->find('#main .prop-info-content',0);
            $body = !empty($bd_ob) ? trim($bd_ob->plaintext) : '';

            $mogi->body = $body;
            $mogi->run = 1;
            $mogi->save();

            MogiDetail::where('mogi_id',$mogi->id)->delete();
            MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>"title",'value'=>$title]);
            MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>"image_cover",'value'=>$image_cover]);
            MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>"address",'value'=>$address]);
            foreach ($details as $label => $detail){
                MogiDetail::insert(['mogi_id'=>$mogi->id,'label'=>$label,'value'=>$detail]);
            }
        }

        echo 'done';
    }

    function crawl(){
        $this->setConfigNoLimit();
        $page = 990;

        while(1){
            if($page > 3334) break;
            $url = "https://mogi.vn/mua-nha-dat?cp=$page";
            $html =  $this->curl($url,'get');
            $dom = HtmlDomParser::str_get_html($html);

            if(empty($dom)) die;

            $lis = $dom->find('.props li');

            foreach ($lis as $li) {
                $a = $li->find('a',0);
                if(empty($a)) continue;

                $href = $a->href;
                $arr = explode('-',$href);
                $mogi_id = str_replace('id','',$arr[count($arr) - 1]);

                if(!Mogi::where('mogi_id',$mogi_id)->exists())
                    Mogi::insert([
                        'url'=>$href,
                        'mogi_id'=>$mogi_id
                    ]);
            }

            $page++;
        }

        echo 'done';
    }
}
