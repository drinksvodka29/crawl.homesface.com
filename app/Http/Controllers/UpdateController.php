<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\OptionItem;
use App\Models\Project;
use App\Models\Projects;
use App\Models\Province;
use App\Models\Schools;
use App\Models\Ward;
use App\RealEstate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UpdateController extends Controller
{
    function latLng(){
        $this->setConfigNoLimit();
      //  $google_key = 'AIzaSyBu2GiSoW2lLEk9mKKygCWjw_v8fW5n_Do';
        $google_key = 'AIzaSyDhbCegVXmJAZlQN-wwl9C_9g7blbX3Vos';
        $k = '9cbf0bc15d3901b7e043d8f76be8d73f370a82fe629a2d46';

        $res = RealEstate::where('lat',0)->where('ward_id',"!=",0)->limit(10000)->get();
        foreach ($res as $id => $re){
            $address = $re->address;
            if(empty($address)) continue;

            $province = Province::where('id',$re->province_code)->first();
            $district = District::where('id',$re->district_id)->first();
            $ward = Ward::where('id',$re->ward_id)->first();

            if(empty($province)) continue;
            if(empty($district)) continue;
            if(empty($ward)) continue;

            $a = $ward->_prefix." ".$ward->_name.", ".trim($district->prefix." ".$district->name).", ".$province->name;

            $address .= ", ".$a;

            $dd = http_build_query([
                 "apikey"=>$k,
                 "size"=>1,
                 "categories"=>'',
                 "api-version"=>'1.1',
                 "text"=>$address,
            ]);
//            $response = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$google_key");
            $response = file_get_contents("https://maps.vietmap.vn/api/search?$dd");
            $response = json_decode($response,true);

            var_dump($response,$address,$dd);die;
            if($response['status'] == 'OK'){
                $re->lat = $response['results'][0]['geometry']['location']['lat'];
                $re->lng = $response['results'][0]['geometry']['location']['lng'];
                $re->save();
            }else if($response['status'] == "ZERO_RESULTS"){
                $re->lat = -1;
                $re->lng = -1;
                $re->save();
            }else{
                var_dump($response);die;
            }
        }

        echo 'done';
    }

    function getAddressNumber(){
        $rand = rand(1,10);
        if($rand <=3)
            $rs = rand(150,1500);
        else
            $rs = rand(150,1500)."/".rand(5,80);

        return $rs;
    }

    function projects(){
        Project::truncate();
        $records = Projects::all();
        foreach ($records as $record){
            Project::insert([
                'title'=>$record->title,
                'province_code'=>$record->province_code,
                'district_id'=>$record->district_id,
                'lat'=>$record->lat,
                'lng'=>$record->lng,
                'slug' => $record->slug,
                'cover_image' => $record->cover_image,
                'acreage' => $record->acreage,
                'size' => $record->size,
                'owner' => $record->owner,
                'owner_url' => $record->owner_url,
            ]);
        }

        echo 'done';
    }

    function project(){
        Projects::truncate();
        $faker = Faker::create('App\Article');
        $records = Project::all();
        foreach ($records as $record){
            $image = rand(0,20);
            if(!$image) $image = '.jpg';
            else $image = "-$image.jpg";

            $slug = $this->slug($record->name);
            if(Projects::where('slug',$slug)->exists())
                $slug = $slug.'-'.rand(1,100000);
            Projects::insert([
                'title'=>$record->name,
                'province_code'=>$record->province_code,
                'district_id'=>$record->district_id,
                'lat'=>$record->lat,
                'lng'=>$record->lng,
                'slug' => $slug,
                'cover_image' => "project/cover_images/product$image",
                'acreage' => rand(30,150),
                'size' => rand(20,70),
                'owner' => $faker->name,
                'owner_url' => $faker->url,
            ]);
        }

        echo 'done';
    }

    function vitri(){
        $google_key = 'AIzaSyBj0XTyrasrhnJnK4zAmf87l223kWyTnok';

        $records = DB::select(
            "SELECT * FROM `schools` WHERE length(dia_chi) > 0 AND dia_chi != 'None' AND lat = 0"
        );

        foreach ($records as $record){
            $address = str_replace(" ", "%20", $record->dia_chi);
            $response = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$google_key");
            $response = json_decode($response,true);
            if($response['status'] == 'OK'){
                Schools::where('id',$record->id)->update([
                    'lat'=>$response['results'][0]['geometry']['location']['lat'],
                    'lng'=>$response['results'][0]['geometry']['location']['lng'],
                ]);
            }
        }

        echo 'done';
    }

    function updateOptionItem(){
        OptionItem::truncate();
        $records = Schools::all();
        foreach ($records as $record){
            OptionItem::insert([
                'option_id'=>11,
                'province_code'=>'SG',
                'district_id'=>$record->district_id,
                'name'=>$record->ten_don_vi,
                'address'=>$record->dia_chi,
                'lat'=>$record->lat,
                'lng'=>$record->lng,
                'rating'=>1,
                'type'=>$record->the_loai,
            ]);
        }

        echo 'done';
    }
}
