<?php

namespace App\Http\Controllers;

use App\Direction;
use App\Models\ChoTot\ChoTot;
use App\Models\ChoTot\ChoTotDetail;
use App\RealEstate;
use App\RealEstateCategory;
use App\RealEstateGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChoTotController extends Controller
{
    function import(){
        $this->setConfigNoLimit();
        $chotots = ChoTot::where('run',1)->where('import',0)->limit(5000)->get();

        foreach ($chotots as $chotot){
            //  $chotot = ChoTot::where('id',18459983)->first();
            if(empty($chotot)) return;

            $re = RealEstate::where('cho_tot_id',$chotot->id)->first();
            if(empty($re))
                $re = new RealEstate();
            else{
                $file = env('RESOURCE_PATH') . $re->cover_image;
                if (!empty($re->cover_image) && file_exists($file))
                    unlink($file);

                RealEstateCategory::where('real_estate_id',$re->id)->delete();
                $real_estate_galleries = RealEstateGallery::where('real_estate_id',$re->id)->get();
                foreach ($real_estate_galleries as $gallery){
                    $file = env('RESOURCE_PATH') . $gallery->url;
                    if (!empty($re->cover_image) && file_exists($file))
                        unlink($file);
                }
                RealEstateGallery::where('real_estate_id',$re->id)->delete();
            }

            $details = ChoTotDetail::where('cho_tot_id',$chotot->id)->get();
            $district_name = '';
            $ward_name = '';
            $category_name = '';
            $cover_image = '';
            $images = [];

            foreach ($details as $detail){
                $value = $detail->value;
                switch ($detail->code){
                    case "subject":
                        $re->title = $value;
                        $re->keyword = $value;
                        $slug = $this->slug($re->title);
                        if(empty($re->id) && RealEstate::where('slug',$slug)->exists())
                            $re->slug = $this->slug($re->title).'-'.time().'-'.rand(1,1000);
                        else
                            $re->slug = $this->slug($re->title);
                        break;
                    case "longitude":
                        $re->lng =$value;
                        break;
                    case "latitude":
                        $re->lat = $value;
                        break;
                    case "price":
                        $re->price = $value;
                        break;
                    case "size":
                        $re->acreage = $value;
                        break;
                    case "address":
                        $re->address = $value;
                        $re->street = $value;
                        break;
                    case "ad_params_direction":
                        $direction = Direction::where('name',$value)->first();
                        if(!empty($direction))
                            $re->direction_code = $direction->code;
                        break;
                    case "street_number":
                        $re->address_number = $value;
                        break;
                    case "toilets":
                        $re->so_luong_nha_ve_sinh = $value;
                        break;
                    case "rooms":
                        $re->so_luong_phong_ngu = $value;
                        break;
                    case "floornumber":
                        $re->so_tang = $value;
                        break;
                    case "ad_params_property_legal_document":
                        if($value == "Đã có sổ đỏ/sổ hồng")
                            $re->house_certificate = 1;
                        break;
                    case "type_name":
                        if($value == "Cần bán")
                            $re->status = 1;
                        else
                            $re->status = 2;
                        break;
                    case "ad_params_region":
                        $province_code = '';
                        if($value == "Hà Nội")
                            $province_code = 'HN';
                        else if($value == "Bà Rịa - Vũng Tàu")
                            $province_code = "VT";
                        else{
                            $q = DB::select(
                                "SELECT code FROM province WHERE '$value' like CONCAT('%',name,'%')"
                            );
                            if(!empty($q))
                                $province_code = $q[0]->code;
                        }
                        $re->province_code = $province_code;
                        break;
                    case "ad_params_area":
                        $district_name = $value;
                        break;
                    case "ad_params_ward":
                        $ward_name = $value;
                        break;
                    case "category_name":
                        $category_name = $value;
                        break;
                    case "thumbnail_image":
                        $cover_image = $value;
                        break;
                    case "images":
                        $images[] = $value;
                        break;
                    case "list_time":
                        $re->created_at = date('Y-m-d H:i:s', $value / 1000);
                        break;
                    default:
                        break;
                }
            }
            $re->body = $chotot->body;
            $re->cho_tot_id = $chotot->id;
            $re->updated_at = date('Y-m-d H:i:s', time());

            $q = DB::select(
                "SELECT id FROM district WHERE ? like CONCAT('%',name,'%') AND province_code = ?"
                ,[$district_name,$re->province_code]);
            if(!empty($q))
                $re->district_id = $q[0]->id;

            $q = DB::select(
                "SELECT id FROM ward WHERE ? like CONCAT('%',_name,'%') AND _district_id = ?"
                ,[$ward_name,$re->district_id]);
            if(!empty($q))
                $re->ward_id = $q[0]->id;

            $re->user_id = 1;
            $re->save();

            try{
                $arr = explode('.',$cover_image);
                $ext = $arr[count($arr) - 1];
                $fName = $re->id."-".time().".$ext";
                $content = file_get_contents($cover_image);

                $re->cover_image = "real_estates/cover_images/$fName";
                $re->save();
                file_put_contents(env('RESOURCE_PATH')."real_estates/cover_images/$fName", $content);
            }catch (\Exception $exception){
//                var_dump($re->id,$re->cho_tot_id,$exception->getMessage());
//                die;
            }

//            foreach ($images as $in => $image){
//                $arr = explode('.',$image);
//                $ext = $arr[count($arr) - 1];
//                if(strlen($ext) > 5) $ext = 'jpg';
//                $fName = $re->id."-$in-".time().".$ext";
//                $content = file_get_contents($image);
//                try{
//                    file_put_contents(env('RESOURCE_PATH')."real_estates/galleries/$fName", $content);
//                }catch (\Exception $exception){
//                    var_dump($image);
//                    die;
//                }
//
//
//                RealEstateGallery::insert([
//                    'real_estate_id'=>$re->id,
//                    'url'=>"real_estates/galleries/$fName",
//                    'name'=>$re->title
//                ]);
//            }
            $chotot->import = 1;
            $chotot->save();

            $real_estate_category = new RealEstateCategory();
            if(!empty($category_name)){
                if($category_name == "Căn hộ/Chung cư")
                    $real_estate_category->category_id = 2;
                else if($category_name == "Đất")
                    $real_estate_category->category_id = 1;
                else if($category_name == "Nhà ở")
                    $real_estate_category->category_id = 3;
                else if($category_name == "Phòng trọ")
                    $real_estate_category->category_id = 16;
                else if($category_name == "Văn phòng, Mặt bằng kinh doanh")
                    $real_estate_category->category_id = 17;

                $real_estate_category->real_estate_id = $re->id;
                $real_estate_category->save();
            }
        }

        echo "done";
    }

    function crawl(){
        $this->setConfigNoLimit();
        $page = 890;
        while(true){
            $limit = 50;
            $offset = ($page - 1) * $limit;
            $url = "https://gateway.chotot.com/v1/public/ad-listing?cg=1000&limit=$limit&o=$offset";
            $results = $this->curl($url,'get');

            $decode = json_decode($results,true);

            if(!empty($decode['ads']))
                foreach ($decode['ads'] as $ad)
                    if(!ChoTot::where('id',$ad['list_id'])->exists())
                        ChoTot::insert([
                            'id'=>$ad['list_id']
                        ]);
            else
                break;

            $page++;
        }

        echo 'done';
    }

    function crawlDetail(){
        $this->setConfigNoLimit();
        $chotots = ChoTot::where('run',0)->limit(4000)->get();

        foreach ($chotots as $chotot){
            $id = $chotot->id;
            $url = "https://gateway.chotot.com/v1/public/ad-listing/$id";
            $results = $this->curl($url,'get');
            $decode = json_decode($results,true);

            if(!isset($decode['ad'])){
                $chotot->run = -1;
                $chotot->save();
                continue;
            }

            $chotot->body = $decode['ad']['body'];
            $chotot->save();

            ChoTotDetail::where('cho_tot_id',$chotot->id)->delete();
            foreach ($decode['ad'] as $code => $value){
                try{
                    if($code == "body") continue;
                    if(is_array($value)){
                        foreach ($value as $item){
                            if(!is_array($item))
                                ChoTotDetail::insert([
                                    'cho_tot_id'=>$chotot->id,
                                    'code'=>$code,
                                    'value'=>$item
                                ]);
                        }
                    }else
                        ChoTotDetail::insert([
                            'cho_tot_id'=>$chotot->id,
                            'code'=>$code,
                            'value'=>$value
                        ]);
                }catch (\Exception $exception){
                    var_dump($chotot->id,$code,$exception->getMessage());die;
                }
            }

            foreach ($decode['ad_params'] as $code => $item){
                try{
                    if(isset($item['value']))
                        ChoTotDetail::insert([
                            'cho_tot_id'=>$chotot->id,
                            'code'=>"ad_params_$code",
                            'value'=>$item['value']
                        ]);
                }catch (\Exception $exception){
                    var_dump($chotot->id,$code,$exception->getMessage());die;
                }
            }

            $chotot->run = 1;
            $chotot->save();
        }

        echo "done";
    }
}
