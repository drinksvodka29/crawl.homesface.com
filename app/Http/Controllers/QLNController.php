<?php

namespace App\Http\Controllers;

use App\Homesface\CompanyManagement;
use App\Models\QLN\Khuvuc;
use App\Modes\QLN\DonViQuanLy;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;

class QLNController extends Controller
{
    function import(){
        $donvi_quanlys = DonViQuanLy::get();
        CompanyManagement::truncate();

        foreach ($donvi_quanlys as $donvi_quanly){
            CompanyManagement::insert([
                'name'=>$donvi_quanly->name,
                'address'=>$donvi_quanly->diachi,
            ]);
        }

        echo 'done';
    }

    function khuvuc(){
        Khuvuc::truncate();
        $url = 'http://www.quanlynha.gov.vn/Desktop.aspx/Tin_tuc-Su_kien/Tin-tong-hop/Cac_don_vi_du_dieu_kien_quan_ly_van_hanh_nha_chung_cu/';
        $page = $this->curl($url,'get');

        $dom = HtmlDomParser::str_get_html($page);

        $tables = $dom->find('#vie_ctl03_ctl00_Content table table tbody');

        foreach ($tables as $table){
            $strong = $table->find('strong');
            if(empty($strong))
                continue;

            $khuvuc = $strong[0]->plaintext;
            $a = $table->find('a');
            foreach ($a as $item){
                $url = "http://www.quanlynha.gov.vn".trim($item->href);
                if(!Khuvuc::where('url',$url)->exists())
                    Khuvuc::insert([
                        'url'=>$url,
                        'name'=>trim($item->plaintext),
                        'khuvuc'=>trim($khuvuc),
                    ]);
            }
        }
        echo 'done';
    }

    function donvi(){
        $this->setConfigNoLimit();
  //      DonViQuanLy::truncate();

        $khuvuc = Khuvuc::where('run',0)->get();
        foreach ($khuvuc as $item){
            $page = $this->curl($item->url,'get');
            $dom = HtmlDomParser::str_get_html($page);
            $trs = $dom->find('.MsoNormalTable tr');
            $tp = $dom->find('.cssDefaultTitle');
            $tp = !empty($tp) ? trim($tp[0]->plaintext) : '';

            foreach ($trs as $index => $tr){
                if(!$index) continue;
                $td = $tr->find('td');
                $name = isset($td[1]) ? trim($td[1]->plaintext) : '';
                $diachi = isset($td[2]) ? trim($td[2]->plaintext) : '';

                DonViQuanLy::insert([
                    'khuvuc'=>$item->khuvuc,
                    'name'=>$name,
                    'diachi'=>$diachi,
                    'thanh_pho'=>$tp,
                ]);
            }

            $item->run = 1;
            $item->save();
        }

        echo 'done';
    }
}
