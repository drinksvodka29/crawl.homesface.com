<?php

namespace App\Http\Controllers;

use App\Models\ToursComplete\BookingDetail;
use App\Models\ToursComplete\Bookings;
use App\Models\ToursComplete\Project;
use App\Models\ToursComplete\Resources;
use Illuminate\Http\Request;
use Faker\Factory as Faker;

class ToursCompleteController extends Controller
{
    function bookings(){
        $this->setConfigNoLimit();

        $faker = Faker::create('App\Article');
        $bookings = Bookings::where('run',0)->get();

        foreach ($bookings as $booking){
            $guest = json_decode($booking->guest,true);
            $name = $faker->name;
            $n_arr = explode(' ',$name);
            if(strpos($n_arr[0],'.') !== false){
                $first_name = $n_arr[1];
                $last_name = $n_arr[2];
            }else{
                $first_name = $n_arr[0];
                $last_name = $n_arr[1];
            }

            BookingDetail::where('booking_id',$booking->id)->where('code','firstname')->update(['value'=>$first_name]);
            BookingDetail::where('booking_id',$booking->id)->where('code','lastname')->update(['value'=>$last_name]);
            BookingDetail::where('booking_id',$booking->id)->where('code','fullname')->update(['value'=>"$first_name $last_name"]);
            BookingDetail::where('booking_id',$booking->id)->where('code','email')->update(['value'=>$faker->email]);
            BookingDetail::where('booking_id',$booking->id)->where('code','phone')->update(['value'=>$faker->phoneNumber]);

            $i = 0;
            $plus = 0;
            if(!empty($guest['data']))
                foreach ($guest['data'] as &$groups){
                    foreach ($groups as &$group){
                        if(!isset( $group['first_name'])) continue;
                        if(!$i){
                            $group['first_name'] = $first_name;
                            $group['last_name'] = $last_name;
                            $group['full_name'] = "$first_name $last_name";
                        }else{
                            $name = $faker->name;
                            $n_arr = explode(' ',$name);
                            if(strpos($n_arr[0],'.') !== false){
                                $first_name = $n_arr[1];
                                $last_name = $n_arr[2];
                            }else{
                                $first_name = $n_arr[0];
                                $last_name = $n_arr[1];
                            }

                            $group['first_name'] = $first_name;
                            $group['last_name'] = $last_name;
                            $group['full_name'] = "$first_name $last_name";
                        }

                        $group['deposit'] = floatval($group['deposit']) + 25;
                        $plus += 25;

                        if (isset($group['base_price']))
                            $group['base_price'] = floatval($group['base_price']) + 25;

                        $i++;
                    }
            }

            Bookings::where('id',$booking->id)->update([
                'guest'=>json_encode($guest),
                'run'=>1,
                'total'=>$booking->total + $plus
            ]);
        }

        echo 'done';
    }

    function resources(){
        $faker = Faker::create('App\Article');
        $resources = Resources::all();

        foreach ($resources as $resource){
            $name = $faker->name;
            $n_arr = explode(' ',$name);
            $nickname = count($n_arr) ? (strpos($n_arr[0],'.') !== false && isset($n_arr[1]) ? $n_arr[1] : $n_arr[0]) : '';
            Resources::where('id',$resource->id)->update([
                'fullname'=>$name,
                'nickname'=>$nickname,
                'email'=>$faker->email,
                'phone'=>$faker->phoneNumber,
            ]);
        }

        echo 'done';
    }

    function projects(){
        Project::where('id',1)->update(['public_url'=>'foodtour']);
        Project::where('id',3)->update(['public_url'=>'citytour']);
        Project::where('id',5)->update(['public_url'=>'transfers']);
        echo 'done';
    }
}
