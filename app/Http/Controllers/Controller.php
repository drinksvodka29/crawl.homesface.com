<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function setConfigNoLimit(){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3600000);
        ini_set('xdebug.max_nesting_level', 9999);
    }

    function debug($data, $die = true){
        print "<pre>";print_r($data);print"</pre>";
        if($die){
            die;
        }
    }

    function curl($url, $type = 'post', $proxy = null){
        $ch = curl_init( $url);

        if(!is_null($proxy) && $proxy != -1){
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }

        if($type != 'post'){
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0';

        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    function slug($text){
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
