<?php

namespace App\Http\Controllers;

use App\Homesface\Builder;
use App\Models\NVTT\NhaThau;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;

class NVTTController extends Controller
{
    function import(){
        $nhathaus = NhaThau::get();
        Builder::truncate();

        foreach ($nhathaus as $nhathau){
            Builder::insert([
                'name'=>$nhathau->name,
                'logo'=>$nhathau->logo,
                'address'=>$nhathau->address,
                'phone'=>$nhathau->phone,
                'website'=>$nhathau->website,
                'type'=>$nhathau->type,
            ]);
        }

        echo 'done';
    }

    function nhathau(){
        $this->setConfigNoLimit();
        NhaThau::truncate();

        $paging = 1;
        while(1){
            if($paging > 13) break;
            $url = "http://trangvangtructuyen.vn/c3/nha-thau-xay-dung/page/$paging/";
            $page = $this->curl($url,'get');
            // echo $page;die;
            $dom = HtmlDomParser::str_get_html($page);

            $div = $dom->find('.div-home');
            if(empty($div)) return 0;

            $div = $div[0];

            $chitiets = $div->find('.chitiet');

            foreach ($chitiets as $chitiet){
                $tencty = $chitiet->find('.tencty h3');
                if(empty($tencty)) continue;
                $tencty = $tencty[0]->plaintext;
                $diachi = $chitiet->find('.diachi');
                $diachi = !empty($diachi) ? trim($diachi[0]->plaintext) : '';
                $logo = $chitiet->find('.logocty img');
                $logo = !empty($logo) ? trim($logo[0]->src) : '';

                $infos = $chitiet->find('.thongtin div');
                $info = [];
                foreach ($infos as $ii_info){
                    $info[] = trim($ii_info->plaintext);
                }

                NhaThau::insert([
                    'name'=>$tencty,
                    'address'=>$diachi,
                    'logo'=>$logo,
                    'phone'=>isset($info[0]) ? $info[0] : '',
                    'fax'=>isset($info[1]) ? $info[1] : '',
                    'email'=>isset($info[2]) ? $info[2] : '',
                    'website'=>isset($info[3]) ? $info[3] : '',
                    'type'=>isset($info[4]) ? $info[4] : '',
                ]);
            }
            $paging++;
        }

        echo 'done';
    }
}
