<?php

namespace App\Http\Controllers;

use App\Models\chudautu\ChuDauTu;
use App\Models\chudautu\ChuDauTuLink;
use App\Models\chudautu\Investor;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;

class ChuDauTuController extends Controller
{
    function convert(){
        Investor::truncate();
        $chudautus = ChuDauTu::all();
        foreach ($chudautus as $chudautu){
            Investor::insert([
                'name'=>$chudautu->ten,
                'full_name'=>$chudautu->ten_day_du,
                'address'=>$chudautu->dia_chi,
                'alias'=>str_replace('https://rever.vn/chu-dau-tu/','',$chudautu->url),
                'logo'=>"investor/logo/image_".$chudautu->id.'.jpg',
                'founded_in'=>$chudautu->thanh_lap.'-01-01',
                'website'=>$chudautu->website,
                'description'=>$chudautu->description,
            ]);
        }

        echo 'done';
    }

    function image(){
        $chudautus = ChuDauTu::where('run',0)->get();
        foreach ($chudautus as $chudautu){
            $img = public_path("investor/logo/image_".$chudautu->id.'.jpg');
            file_put_contents($img, file_get_contents($chudautu->logo_url));

            ChuDauTu::where('id',$chudautu->id)->update([
                'logo'=>"investor/logo/image_".$chudautu->id.'.jpg',
                'run'=>1
            ]);
        }

        echo 'done';
    }

    function crawl(){
        $chudautulinks = ChuDauTuLink::where('run',0)->get();

        foreach ($chudautulinks as $chudautulink){
            $page = $this->curl($chudautulink->url,'get');
            $dom = HtmlDomParser::str_get_html($page);

            $name_ob = $dom->find('#developer-short-name');
            $name = !empty($name_ob) ? $name_ob[0]->value : '';

            $full_name_ob = $dom->find('#developer-name');
            $full_name = !empty($full_name_ob) ? $full_name_ob[0]->value : '';

            $description_ob = $dom->find('.developer-description');
            $description = !empty($description_ob) ? $description_ob[0]->plaintext : '';

            $developer_info_ob = $dom->find('.developer-info tr');
            $info = [];
            foreach ($developer_info_ob as $item){
                $label_ob = $item->find('.light');
                $label = !empty($label_ob) ? $label_ob[0]->innertext() : '';

                $value_ob = $item->find('.bold');
                $value = !empty($value_ob) ? $value_ob[0]->innertext() : '';

                $info[$label] = $value;
            }

            $logo_ob =  $dom->find('.developer-full-img img');
            $logo = !empty($logo_ob) ? $logo_ob[0]->src : '';

            $data = [
                'url'=>$chudautulink->url,
                'ten'=>$name,
                'ten_day_du'=>$full_name,
                'description'=>$description,
                'logo_url'=>$logo,
                'thanh_lap'=>isset($info['Thành lập:']) ? $info['Thành lập:'] : 0,
                'dia_chi'=>isset($info['Địa chỉ:']) ? $info['Địa chỉ:'] : '',
                'website'=>isset($info['Website:']) ? $info['Website:'] : '',
            ];

            if(!ChuDauTu::where('url',$chudautulink->url)->exists())
                ChuDauTu::insert($data);
            else
                ChuDauTu::where('url',$chudautulink->url)->update($data);

            ChuDauTuLink::where('url',$chudautulink->url)->update(['run'=>1]);
        }

        echo 'done';
    }

    function index(){
        ChuDauTuLink::truncate();
        $p = 1;
        while (1){
            $page = $this->curl("https://rever.vn/chu-dau-tu?page=$p",'get');
            $dom = HtmlDomParser::str_get_html($page);

            // echo $dom;die;
            if(empty($dom)) die('error');

            $developer_list = $dom->find('.developer-list .items');
            if(!count($developer_list)) break;

            foreach ($developer_list as $item){
                $a = $item->find('.dev-logo');
                if(!empty($a)){
                    $url = 'https://rever.vn'.$a[0]->href;
                    if(!ChuDauTuLink::where('url',$url)->exists())
                        ChuDauTuLink::insert([
                            'url'=>$url
                        ]);
                }
            }

            $p++;
        }

        echo 'done';
    }
}
