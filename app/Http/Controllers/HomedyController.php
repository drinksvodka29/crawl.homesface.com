<?php

namespace App\Http\Controllers;

use App\HomedyCategory;
use App\HomedyPlaces;
use Illuminate\Http\Request;

class HomedyController extends Controller
{
    function place($category_id){
        $this->setConfigNoLimit();
     //   HomedyPlaces::truncate();
       // $category_id = 1;

      //  while(1){
            $page_index = 1;

            while (1){
                $response = file_get_contents("https://service.homedy.com/Place/Search?CategoryId=$category_id&Latitude=10.3766583&Longitude=107.1254998&Distance=20000000&PageIndex=$page_index&PageSize=100");
                $decode = json_decode($response,true);
                if(empty($decode['data']))
                    break;

                foreach ($decode['data'] as $datum){
                    if(!HomedyPlaces::where('origin_id',$datum['id'])->exists())
                        HomedyPlaces::insert([
                            'origin_id'=>$datum['id'],
                            'title'=>$datum['title'],
                            'brand'=>$datum['brand'],
                            'address'=>$datum['address'],
                            'category_id'=>$datum['categoryId'],
                            'rating'=>$datum['rating'],
                            'description'=>$datum['description'],
                            'image'=>$datum['image'],
                            'phone'=>$datum['phone'],
                            'email'=>$datum['email'],
                            'facebook'=>$datum['facebook'],
                            'lat'=>$datum['lat'],
                            'lng'=>$datum['long'],
                            'geo_location'=>$datum['geoLocation'],
                            'category_name'=>$datum['categoryName'],
                           // 'encode_data'=>$response,
                        ]);
                }

                $page_index++;
            }

//            if($category_id > 30)
//                break;
//
//            $category_id++;
     //   }

        echo 'done';
    }

    function category(){
        HomedyCategory::truncate();

        for($i = 1; $i <= 30; $i++){
            $response = file_get_contents("https://service.homedy.com/place/getplaces?categoryId=$i");

            $decode = json_decode($response,true);
            foreach ($decode['categories'] as $item){
                if(!HomedyCategory::where('id',$item['Id'])->exists())
                    HomedyCategory::insert([
                        'id'=>$item['Id'],
                        'name'=>$item['Name'],
                    ]);
            }
        }

        echo 'done';
    }
}
