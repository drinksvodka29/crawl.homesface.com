<?php

namespace App\Http\Controllers;

use App\Model\MamNon;
use App\Models\KhoiKhac;
use App\Models\Schools;
use App\Models\TieuHoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CrawlController extends Controller
{
    function index(){
        Schools::truncate();
        // khoi khac
        $this->crawl('edu_ds_donvi_khoi_khac_1','KHOI_KHAC');
        // mam non
        $this->crawl('edu_ds_donvi_khoi_mam_non_1','MAM_NON');
        // tieu hoc
        $this->crawl('edu_ds_donvi_khoi_tieu_hoc_0','TIEU_HOC');
        // trung cap
        $this->crawl('edu_ds_donvi_khoi_ttgdtx_1','TRUNG_CAP');
        // thcs
        $this->crawl('edu_ds_donvi_khoi_thcs_1','THCS');
        // thph tu thuc
        $this->crawlTHPT('edu_ds_donvi_thpt_tu_thuc','THPT_TU_THUC');
        // thph cong lap
        $this->crawlTHPT('edu_ds_donvi_thpt_cong_lap_0','THPT_CONG_LAP');

        echo 'done';
    }

    function crawl($file,$type){
        $file_n = storage_path("app/crawl/$file.csv");
        $file = fopen($file_n, "r");
        $i = -1;
        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE) {
            $i++;
            if(!$i) continue;

            Schools::insert([
                'parent_id'=>$data[0],
                'ten_don_vi'=>$data[1],
                'ma_don_vi'=>$data[2],
                'dia_chi'=>$data[3],
                'ten_mien'=>$data[4],
                'the_loai'=>$type
            ]);
        }
        fclose($file);
    }

    function crawlTHPT($file,$type){
        $file_n = storage_path("app/crawl/$file.csv");
        $file = fopen($file_n, "r");
        $i = -1;
        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE) {
            $i++;
            if(!$i) continue;

            Schools::insert([
                'ten_don_vi'=>$data[0],
                'dia_chi'=>$data[1],
                'lien_cap'=>$data[2],
                'ten_mien'=>$data[3],
                'the_loai'=>$type
            ]);
        }
        fclose($file);
    }
}
