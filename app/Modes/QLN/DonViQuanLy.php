<?php

namespace App\Modes\QLN;

use Illuminate\Database\Eloquent\Model;

class DonViQuanLy extends Model
{
    protected $table = 'qln_don_vi_quan_ly';
    public $timestamps = false;
}
