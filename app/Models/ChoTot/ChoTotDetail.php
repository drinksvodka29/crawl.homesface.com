<?php

namespace App\Models\ChoTot;

use Illuminate\Database\Eloquent\Model;

class ChoTotDetail extends Model
{
    protected $table = "cho_tot_detail";
    public $timestamps = false;
}
