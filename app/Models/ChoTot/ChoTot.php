<?php

namespace App\Models\ChoTot;

use Illuminate\Database\Eloquent\Model;

class ChoTot extends Model
{
    protected $table = 'cho_tot';
    public $timestamps = false;
}
