<?php

namespace App\Models\QLN;

use Illuminate\Database\Eloquent\Model;

class Khuvuc extends Model
{
    public $timestamps = false;
    protected $table = 'qln_khu_vuc';
}
