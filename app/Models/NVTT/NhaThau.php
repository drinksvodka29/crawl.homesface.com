<?php

namespace App\Models\NVTT;

use Illuminate\Database\Eloquent\Model;

class NhaThau extends Model
{
    public $timestamps = false;
    protected $table = 'tvtt_nha_thau';
}
