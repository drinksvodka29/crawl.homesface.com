<?php

namespace App\Models\chudautu;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    public $timestamps = false;
    protected $table = 'investor';
}
