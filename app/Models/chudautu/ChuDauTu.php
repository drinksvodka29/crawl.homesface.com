<?php

namespace App\Models\chudautu;

use Illuminate\Database\Eloquent\Model;

class ChuDauTu extends Model
{
    protected $table = 'chudautu';
    public $timestamps = false;
}
