<?php

namespace App\Models\chudautu;

use Illuminate\Database\Eloquent\Model;

class ChuDauTuLink extends Model
{
    public $timestamps = false;
    protected $table = 'chudautu_link';
}
