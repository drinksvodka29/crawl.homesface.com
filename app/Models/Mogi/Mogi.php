<?php

namespace App\Models\Mogi;

use Illuminate\Database\Eloquent\Model;

class Mogi extends Model
{
    protected $table = 'mogi';
    public $timestamps = false;
}
