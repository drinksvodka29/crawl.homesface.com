<?php

namespace App\Models\Mogi;

use Illuminate\Database\Eloquent\Model;

class MogiDetail extends Model
{
    protected $table = 'mogi_detail';
    public $timestamps = false;
}
