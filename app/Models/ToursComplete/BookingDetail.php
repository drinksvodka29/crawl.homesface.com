<?php

namespace App\Models\ToursComplete;

use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{
    protected $table = 'booking_detail';
    public $timestamps = false;
}
