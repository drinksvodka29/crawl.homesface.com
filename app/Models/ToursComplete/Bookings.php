<?php

namespace App\Models\ToursComplete;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    protected $table = 'bookings';
    public $timestamps = false;
}
