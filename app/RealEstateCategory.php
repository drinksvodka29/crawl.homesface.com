<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealEstateCategory extends Model
{
    protected $table = 'real_estate_category';
}
