<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomedyCategory extends Model
{
    protected $table = 'homedy_category';
}
