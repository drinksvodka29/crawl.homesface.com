<?php

namespace App\Homesface;

use Illuminate\Database\Eloquent\Model;

class CompanyManagement extends Model
{
    protected $table = 'com_management';
}
